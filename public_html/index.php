<?php
require_once("config.php");
session_start();
if($_GET != NULL)
    if($_GET["action"] == "debug")
    {
        session_destroy();
        require_once("content/debug.php");
        exit();
    }   

if(!isset($_SESSION["SubjectID"]))
{
   
    $sql = "SELECT max(`Subject ID`) AS SubjectID FROM `Demographic Survey`";
    $result = $conn->query($sql);
    $row = $result->fetch_array(MYSQLI_ASSOC);

    var_dump($row);

    if ($row["SubjectID"] == NULL) 
    {
        $_SESSION["SubjectID"] = 1;
    }
    else
    {
        $_SESSION["SubjectID"] = $row["SubjectID"] + 1;
    }
    $_SESSION["end time"] = time() + 7*60; //Endtime = 10 minuts fromt the begining
    $_SESSION["step"] = 0;
    //var_dump($_SESSION);
    header("Location:index.php");
}
else
{
    //var_dump($_SESSION);
    if($_SESSION["step"] == 0)
         header("Location:content/consent-form.php");
    elseif($_SESSION["step"] == 0.1)
         header("Location:content/hipaa-form.php");
    elseif($_SESSION["step"] == 1.1)
        header("Location:content/demographic-survey.php");
    elseif($_SESSION["step"] == 1.2)
        header("Location:content/twenty-item-toronto-alexithymia-scale-survey.php");
    elseif($_SESSION["step"] == 1.3)
        header("Location:content/life-events-checklist.php");
    elseif($_SESSION["step"] == 1.4)
        header("Location:content/pcl-c.php");
    elseif($_SESSION["step"] == 1.5)
        header("Location:content/interpersonal-reactivity-index.php");
    elseif($_SESSION["step"] == 2)
        header("Location:content/experiment-main.php");
}
?>
