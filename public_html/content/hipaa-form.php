<!DOCTYPE HTML>
<?php
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	require_once("initialize.php");
	$name = $_POST["name"];
	$sql = "INSERT INTO `HIPAA Responses` (`Name`) VALUES ('$name')";
	$result = $conn->query($sql);
	if(!$result)
	{
	echo $conn->error;
	die("Insertion Unsuccessful:".$sql."<br>");
	}
	$_SESSION['step'] = 1.1;
	header("Location:../index.php");
}
?>

<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>
<div id="main">     
    <div id="content">
		<?php
			require_once("initialize.php");
		?>
		<h1> Authorization To Use Personal Health Information In Research </h1>
		<h3>
		Study Title:	Characterizing Face Processing in  Psychiatric Disorders <br>
		Protocol Number: 2013H0145<br>
		Principal Investigator:	Aleix Martinez <br>
		Sponsor	National Eye Institute </h3>
		<h3>Please Read Form Below <a href="consent.pdf">(Download PDF)</a></h3> 
<!-- ******************************************************************************************** -->	
		<div id="" style="overflow-y: scroll; height:500px; width:650px;font-size:15px">
		<p>
			Before researchers use or share any health information about you as part of this study, 
			The Ohio State University is required to obtain your authorization. This helps explain to you how 
			this information will be used or shared with others involved in the study.  
			<ul>
				<li>The Ohio State University and its hospitals, clinics, health-care providers, 
				and researchers are required to protect the privacy of your health information. </li>

				<li>You should have received a Notice of Privacy Practices when you received health care services 
				here.  If not, let us know and a copy will be given to you.  Please carefully review this 
				information. Ask if you have any questions or do not understand any parts of this notice.</li>

				<li>If you agree to take part in this study your health information will be used and
				 shared with others involved in this study. Also, any new health information about 
				 you that comes from tests or other parts of this study will be shared with those 
				 involved in this study.</li>
				
				<li>Health information about you that will be used or shared with others involved in this study may
				 include your research record and any health care records at The Ohio State University.
				 For example, this may include your medical records, x-rays, or laboratory results. 
				 Psychotherapy notes in your health records (if any) will not, however, be shared or used. 
				 Use of these notes requires a separate, signed authorization.</li>
			</ul>		
			Please read the information carefully before signing this form. 
			Please ask if you have any questions about this authorization, the university’s 
			Notice of Privacy Practices or the study before signing this form.
			<h3>Those Who May Use, Share, and Receive Your Information as Part of This Study</h3>
			<ul> 
				<li>Researchers and staff at The Ohio State University will use, share, and receive your 
					personal health information for this research study. Authorized Ohio State staff not 
					involved in the study may be aware that you are participating in a research study and 
					have access to your information. If this study is related to your medical care, your 
					study-related information may be placed in your permanent hospital, clinic, or physician’s 
					office records. </li>
				<li>Those who oversee the study will have access to your information, including the following:
					<ul>
						<li>Members and staff of The Ohio State University’s Institutional Review Boards, including the Western Institutional Review Board<li>
						<li>The Ohio State University Office of Responsible Research Practices.</li>
						<li>University data safety monitoring committees.</li>
						<li>The Ohio State University Office of Research.</li>
					</ul>
					</li>
				<li>Your health information may also be shared with federal and state agencies that have oversight of the study or to whom access is required under the law. These may include the following: 
					<ul>
						<li>Food and Drug Administration</li>
						<li>Office for Human Research Protections</li>
						<li>National Institutes of Health</li>
						<li>Ohio Department of Job and Family Services</li>
					</ul></li>
				<li>These researchers, companies and/or organization(s) outside of The Ohio State University may also use, share and receive your health information in connection with this study:
					<ul>
						<li>Health care facilities, research site(s), researchers, health care providers, or study monitors involved in this study:  Neuroradiologists in Nationwide Children's Hospital Columbus and Columbus Radiology Corp.</li>
					</ul></li>
			</ul>
			The information that is shared with those listed above may no longer be protected by federal privacy rules.
		</p>
		<h3>Authorization Period</h3>
		<p>
			This authorization will not expire unless you change your mind and revoke it in writing. 
			There is no set date at which your information will be destroyed or no longer used.  
			This is because the information used and created during the study may be analyzed for many 
			years, and it is not possible to know when this will be completed.
		</p>
		<h3>Signing the Authorization</h3>
		<ul>
			<li>You have the right to refuse to sign this authorization.  Your health care outside of the study, payment for your health care, and your health care benefits will not be affected if you choose not to sign this form.</li>
			<li>You will not be able to take part in this study and will not receive any study treatments if you do not sign this form.</li>
			<li>If you sign this authorization, you may change your mind at any time. Researchers may continue to use information collected up until the time that you formally changed your mind.  If you change your mind, your authorization must be revoked in writing.  To revoke your authorization, please write to:  Dr. Aleix Martinez at 614-688-8225 or aleix@ece.osu.edu.</li>
			<li>Signing this authorization also means that you will not be able to see or copy your study-related information until the study is completed. This includes any portion of your medical records that describes study treatment. </li>
		</ul>
	</div>
<!-- ******************************************************************************************** -->	
		<h3>Contacts For Questions</h3>
		<ul>
			<li>If you have any questions relating to your privacy rights, please contact Dr. Karel Smith at 1-614-688-8544 or smith.3513@osu.edu.</li>
			<li>If you have any questions relating to the research, please contact:  Dr. Aleix Martinez at 614-688-8225 or aleix@ece.osu.edu.</li>
		</ul>

		<h3>Signature</h3>
		<p><span style="font-size:17px">
		I have read (or someone has read to me) this form and have been able to ask questions.
		All of my questions about this form have been answered to my satisfaction.  
		By signing below, I permit Dr. Aleix Martinez and the others listed on this form to
		use and share my personal health information for this study.  I will be given a copy of 
		this signed form. </span>

		<form name="form" method="post" action="" onsubmit="return validateForm(this)">
			<p><input type="checkbox" name="checkbox" required> Agree to Terms </p>
			<p>Sign with Name<input class="contact" type="text" name="name" value="" autofocus="off" required ></p>
			<p>Date: <?php 
							date_default_timezone_set('UTC');
							echo date('D, d M Y H:i');

					?>
			</p>
			<input class="submit" type="submit" name="submit" value="submit" >
		</p>
		</form>
    </div>
</div>
<?php    
    require_once("../layout/footer.php");
?>
    
</body>
</html>


