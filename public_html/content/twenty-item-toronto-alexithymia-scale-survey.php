<!DOCTYPE HTML>
<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>
<div id="main">     
    <div id="content">

		<h2>Twenty Item Toronto Alexithymia Scale Survey</h2>

		<form name="htmlform" method="post" action="twenty-item-toronto-alexithymia-scale-survey-save.php" autocomplete="off">
		<table style="width:100%">  
		  <tr>
		  	<td></td>
		  	<td style="text-align:center"><h4>Statement</h4></td>
		  	<td style="text-align:center"><h4>Strongly Disagree</h4></td>
		  	<td style="text-align:center"><h4>Disagree</h4></td>
		  	<td style="text-align:center"><h4>Neither Agree nor Disagree</h4></td>
		  	<td style="text-align:center"><h4>Agree</h4></td>
		  	<td style="text-align:center"><h4>Strongly Agree</h4></td>
		  </tr>
		  <tr>
		  	<td>1</td>
		    <td>I am often confused about what emotion I am feeling.</td>
		    <td style="text-align:center"><input type="radio" name="1" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="1" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="1" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="1" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="1" value="5">5</td>
		  </tr>
		  <tr>
		  	<td>2</td>
		    <td>It is difficult for me to find the right words for my feelings.</td>
		    <td style="text-align:center"><input type="radio" name="2" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="2" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="2" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="2" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="2" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>3</td>
		    <td>I have physical sensations that even doctors don't understand.</td>
		    <td style="text-align:center"><input type="radio" name="3" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="3" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="3" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="3" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="3" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>4</td>
		    <td>I am able to describe my feeling easily.</td>
		    <td style="text-align:center"><input type="radio" name="4" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="4" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="4" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="4" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="4" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>5</td>
		    <td>I prefer to analyze problems rather than just describe them.</td>
		    <td style="text-align:center"><input type="radio" name="5" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="5" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="5" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="5" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="5" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>6</td>
		    <td>When I am upset, I don't know if I am sad, frightened or angry.</td>
		    <td style="text-align:center"><input type="radio" name="6" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="6" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="6" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="6" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="6" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>7</td>
		    <td>I am often puzzled by sensations in my body.</td>
		    <td style="text-align:center"><input type="radio" name="7" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="7" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="7" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="7" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="7" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>8</td>
		    <td>I prefer to just let things happen rather than to understand why they turned out that way.</td>
		    <td style="text-align:center"><input type="radio" name="8" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="8" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="8" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="8" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="8" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>9</td>
		    <td>I have feelings that I can't quite identify.</td>
		    <td style="text-align:center"><input type="radio" name="9" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="9" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="9" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="9" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="9" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>10</td>
		    <td>Being in touch with emotions is essential.</td>
		    <td style="text-align:center"><input type="radio" name="10" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="10" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="10" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="10" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="10" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>11</td>
		    <td>I find it hard to describe how I feel about people.</td>
		    <td style="text-align:center"><input type="radio" name="11" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="11" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="11" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="11" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="11" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>12</td>
		    <td>People tell me to describe my feelings more.</td>
		    <td style="text-align:center"><input type="radio" name="12" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="12" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="12" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="12" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="12" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>13</td>
		    <td>I don't know what's going on inside me.</td>
		    <td style="text-align:center"><input type="radio" name="13" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="13" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="13" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="13" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="13" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>14</td>
		    <td>I often don't know why I am angry.</td>
		    <td style="text-align:center"><input type="radio" name="14" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="14" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="14" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="14" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="14" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>15</td>
		    <td>I prefer talking to people about their daily activites rather than their feelings.</td>
		    <td style="text-align:center"><input type="radio" name="15" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="15" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="15" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="15" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="15" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>16</td>
		    <td>I prefer to watch "light" entertainment shows rather than psychological dramas.</td>
		    <td style="text-align:center"><input type="radio" name="16" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="16" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="16" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="16" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="16" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>17</td>
		    <td>It is difficult for me to reveal my innermost feeelings, even to close friends.</td>
		    <td style="text-align:center"><input type="radio" name="17" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="17" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="17" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="17" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="17" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>18</td>
		    <td>I can feel close to someone, even in moments of silence.</td>
		    <td style="text-align:center"><input type="radio" name="18" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="18" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="18" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="18" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="18" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>19</td>
		    <td>I find examination of my feelings useful in solving personal problems.</td>
		    <td style="text-align:center"><input type="radio" name="19" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="19" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="19" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="19" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="19" value="5">5</td>
		  </tr>
		  <tr>
		   	<td>20</td>
		    <td>Looking for hidden meaning in movies or plays detracts from their enjoyment.</td>
		    <td style="text-align:center"><input type="radio" name="20" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="20" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="20" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="20" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="20" value="5">5</td>
		  </tr>
		</table>
		<p style="padding-top: 15px;text-align:center "><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Submit" id = "mySubmit"/></p>
	</form>
 	</div>
</div>
<?php    
    require_once("../layout/footer.php");
?>    
</body>
</html>

