<!DOCTYPE HTML>
<html>
<head>
<?php 
    require_once("../layout/experiment-header.php");
    require_once("experiment-initialize.php");
?>
</head>
<body>
<!-- =========================================================================================== -->
<?php
$subjectid = $_SESSION["SubjectID"];
if($_SERVER['REQUEST_METHOD'] == "POST")
{
    //var_dump($_REQUEST);
    $stimulusid = $_REQUEST["stimulusid"];
    $response = $_REQUEST["response"];
    $responsetime = $_REQUEST["responsetime"];
    $sql = "INSERT INTO `Responses` (`Subject ID`, `Stimulus ID`, `Response`, `Response Time`) VALUES ( $subjectid, $stimulusid, '$response', $responsetime)"; 
    $result = $conn->query($sql);
    if(!$result)
    {
        echo $conn->error;
        echo "Insertion Unsuccessful:".$sql."<br>";
    }
}
else
{
    //echo $_SERVER['REQUEST_METHOD'];
    //echo "Form not submitted";
}
    
echo '<div class="container">';
    echo '<div class="row centered">';
        echo '<div class="col-sm-8 centered">';
            $sql = "SELECT MAX(`Stimulus ID`) AS id FROM `Test Images`";
            $result = $conn->query($sql);
            if($conn->error != NULL)
                echo $sql."<br>ERROR:".$conn->error."<br>"; 
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $maxid = $row["id"];

            $sql = "SELECT MIN(`Stimulus ID`) AS id FROM `Test Images`";
            $result = $conn->query($sql);
            if($conn->error != NULL)
                echo $sql."<br>ERROR:".$conn->error."<br>"; 
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $minid = $row["id"];
            $sql = "SELECT * FROM `Responses` WHERE `Subject ID`=$subjectid";
            $result1 = $conn->query($sql);
            if($conn->error != NULL)
                echo $sql."<br>ERROR:".$conn->error."<br>";
            
            $sql = "SELECT * FROM `Test Images` ";
            $result2 = $conn->query($sql);
            if($conn->error != NULL)
                echo $sql."<br>ERROR:".$conn->error."<br>";
            //echo "Number of Images displyed".$result1->num_rows."<br>";
            //echo "Number of Images to be displayed".$result2->num_rows."<br>";
            if($result1->num_rows == $result2->num_rows)
            {    
                $_SESSION["experiment_step"] = 4;
                header("Location:experiment-main.php");
                exit();
            }
            $stimulusid = rand($minid,$maxid); //Make rand get arguments from database itself
            $sql = "SELECT * FROM `Responses` WHERE `Subject ID`=$subjectid AND `Stimulus ID`=$stimulusid";
            $result = $conn->query($sql);
            if($conn->error != NULL)
                echo $sql."<br>ERROR:".$conn->error."<br>";
            while($result->num_rows != 0)
            {
                $stimulusid = rand($minid,$maxid);
                $sql = "SELECT * FROM `Responses` WHERE `Subject ID`=$subjectid AND `Stimulus ID`=$stimulusid";
                $result = $conn->query($sql);
                if($conn->error != NULL)
                    echo $sql."<br>ERROR:".$conn->error."<br>";
            }
            $sql = "SELECT * FROM `Test Images` WHERE `Stimulus ID` = $stimulusid";
            $result = $conn->query($sql);
            if($result->num_rows == 0)
                die("No such Image:$stimulusid");
            else
                $row = $result->fetch_array(MYSQLI_ASSOC);
            echo "<img src=../../resources/db/stimuli/".$row["Address"].">";
        echo '</div>';
        echo '<div class="col-sm-4 centered">';
            echo '<form id="study" name="study" method="post" action="'.$_SERVER['PHP_SELF'].'">';
                echo '<input type="hidden" name="stimulusid" value="'.$stimulusid.'">';
                echo '<input type="hidden" name="responsetime" value="">';
                echo '<div class="btn-group">';
                    $sql = "SELECT * FROM `Expressions`";
                    $result = $conn->query($sql);
                    if($result->num_rows == 0)
                        die("No Expressions Found:$stimulusid");
                   while($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        //var_dump($row);
                        echo "<button name=\"response\" type=\"submit\" class=\"btn btn-experiment\" onclick=\"getResponseTime()\" value = \"".$row['Name']."\">".$row['Name']."</button>";
                    }
                echo '</div>
            </form>
        </div>
    </div>
</div>';
?>
<script>
var starttime = (new Date).getTime();
function getResponseTime(){
    formObject = document.forms['study'];
    formObject.elements['responsetime'].value = (new Date).getTime() - starttime;
    console.log(formObject.elements['responsetime'].value);
    return 0;
}
</script>
<!-- =========================================================================================== -->  
<div class="container">
    <div class="row centered">
        <div class="col-sm-8 centered">
        </div>
        <div class="col-sm-4 centered">
            <?php require_once("countdown.php"); ?>
        </div>
    </div>
</div>

      
</body>
</html>