<?php
  require_once("experiment-initialize.php");
  if(!isset($_SESSION["experiment_step"]))
    header("Location:experiment-1begin.php");
  elseif($_SESSION["experiment_step"] == 1)
    header("Location:experiment-2training.php");
  elseif($_SESSION["experiment_step"] == 2)
    header("Location:experiment-3description.php");
  elseif($_SESSION["experiment_step"] == 3)
    header("Location:experiment-4study.php");
  elseif($_SESSION["experiment_step"] == 4)
    header("Location:experiment-5end.php");
?>

			

