<!DOCTYPE HTML>
<html>
<head>
<?php 
    require_once("../layout/experiment-header.php");
    require_once("experiment-initialize.php");
?>
</head>
<body>
<!-- =========================================================================================== -->
<?php
	$sql = "UPDATE `Experiment Status` SET `Complete Experiment`=true WHERE `Subject ID`=".$_SESSION['SubjectID'];
	$result = $conn->query($sql);
    if(!$result)
    {
        echo $conn->error;
        echo "Insertion Unsuccessful:".$sql."<br>";
    }
	session_unset(); 
	session_destroy();
?>
<div class="jumbotron" style="background-color:#000">
  <h1>Experiment is Over</h1>
  <p>Thank you for your Time</p>
  <p><a class="btn btn-experiment btn-lg" href="../index.php" role="button">Restart</a></p>
</div>
<!-- =========================================================================================== -->  
<div class="container">
    <div class="row centered">
        <div class="col-sm-8 centered">
        </div>
        <div class="col-sm-4 centered">
            <?php require_once("countdown.php"); ?>
        </div>
    </div>
</div>

      
</body>
</html>