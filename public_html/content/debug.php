<?php
if ($conn->connect_error)
{
    echo "Could not connect to Database: $dbname on server:$servername<br>";
    die("Connection failed:" . $conn->connect_error);
}
echo "<h1>List of Tables</h1>";
$sql = "SELECT * FROM information_schema.TABLES WHERE TABLE_SCHEMA = '$dbname'";
$tabresult = $conn->query($sql);

if($tabresult->num_rows > 0)
{
    while($table = $tabresult->fetch_assoc())
    {
        $tablename = $table['TABLE_NAME'];
        $sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '$dbname' AND TABLE_NAME = '$tablename'";
        $result = $conn->query($sql);
        //echo $sql;
        if($result->num_rows > 0)
        {
            echo "<h4>Schema for Table:$tablename</h4><br>";
            echo "<u>Name</u> <u>DATA_TYPE</u> <u>CHARACTER_MAXIMUM_LENGTH</u><br>";
            while($row = $result->fetch_assoc())
                echo "<b>".$row['COLUMN_NAME']."</b> ".$row['DATA_TYPE']." ".$row['CHARACTER_MAXIMUM_LENGTH']."<br>";
        }
        echo "<br>";
    }
}
else
    die("No tables exist in Database $dbname<br>");
echo "<br>";
echo '*********************************************************************************<br>';
/***************************************************************************************/
echo '<h1>Enter an SQL query to execute</h1><br>
      Sample Queries:<br>
      CREATE DATABASE cbcs<br>
      DROP DATABASE cbcs<br>
      CREATE TABLE "Consent Form" ( ID int AUTO_INCREMENT PRIMARY KEY, NAME varchar(200), "Time of Signing" DATETIME default CURRENT_TIMESTAMP)<br>
      INSERT<br>
      DELETE FROM `Test Images` WHERE `Image ID`=1<br>
      <br>';
if($_SERVER['REQUEST_METHOD'] == "POST" && $_GET["do"] == 1)
{
    $query = $_REQUEST["query"];
    $result = $conn->query($query);
            if($conn->error == NULL)
                echo "Sucessfully Executed:".$query;
            else
                echo $query."<br>ERROR:".$conn->error."<br>";      
}
echo '<form method="POST" action="'.$_SERVER["PHP_SELF"].'?action=debug&do=1">';
echo '<input type="text" size=100 name="query">';
echo '<input name="querysubmit" type="submit" value="Execute"><br>';
echo '</form>';
echo '*********************************************************************************';
/***************************************************************************************/

if($_SERVER['REQUEST_METHOD'] == "POST" && $_GET["do"] == 2)
{
    $path = $_REQUEST["path"];
    $files = scandir($path);
    $count = 0;
    foreach ($files as $value)
    {
        if(preg_match("~._?._?.~", $value))
        {
            $parts = preg_split("~_~", $value);
            $sql = "SELECT * FROM `Test Images` WHERE `Address`='$value'";
            $result = $conn->query($sql);
            if($conn->error != NULL)
                echo $query."<br>ERROR:".$conn->error."<br>"; 
            if($result->num_rows == 0)
            {
                $sql = "INSERT INTO `Test Images` (`Expression ID`, `Person ID`, `Address`) VALUES ($parts[0], $parts[1], '$value')";
                $conn->query($sql);
                if($conn->error != NULL)
                    echo $query."<br>ERROR:".$conn->error."<br>";
                else
                {
                    $count = $count + 1;
                    echo "Entered ".$value."<br>";
                }
            }
            else
            {
                echo $value." already Exists<br>";
            }    
        }
    }
    echo "<b>Successfully Entered ".$count." entries.<br>";
}
echo '<h1>Enter a path to populate the Test Images Table</h1>';
echo "Root directory is <i>public_html/</i><br>Do not add back slash in the end.<br>";
echo '<form method="POST" action="'.$_SERVER["PHP_SELF"].'?action=debug&do=2">';
echo '<input type="text" size=100 name="path">';
echo '<input name="pathsubmit" type="submit" value="Submit"><br>';
echo '</form>';
echo '*********************************************************************************<br>';
/***************************************************************************************/
echo '<h1>Populate a Table</h1>';
if($_SERVER['REQUEST_METHOD'] == "POST" && $_GET["do"] == 3)
{
    $tablename = $_REQUEST["tablename"];
    $path = $_REQUEST["path"];
    $files = scandir($path);
    $count = 0;
    foreach ($files as $value)
    {
        if(preg_match("~.png~", $value))
        {
            $sql = "SELECT * FROM `$tablename` WHERE `Address`='$value'";
            $result = $conn->query($sql);
            if($conn->error != NULL)
                echo $query."<br>ERROR:".$conn->error."<br>"; 
            if($result->num_rows == 0)
            {
                $sql = "INSERT INTO `$tablename` (`Address`) VALUES ('$value')";
                $conn->query($sql);
                if($conn->error != NULL)
                    echo $query."<br>ERROR:".$conn->error."<br>";
                else
                {
                    $count = $count + 1;
                    echo "Entered ".$value."<br>";
                }
            }
            else
            {
                echo $value." already Exists<br>";
            }    
        }
    }
    echo "<b>Successfully Entered ".$count." entries.<br>";
}


echo '<form method="POST" action="'.$_SERVER["PHP_SELF"].'?action=debug&do=3">';
echo 'Enter Table Name:<input type="text" size=100 name="tablename"><br>';
echo "Root directory is <i>public_html/</i><br>Do not add back slash in the end.<br>";
echo 'Enter path:<input type="text" size=100 name="path">';
echo '<input name="pathsubmit" type="submit" value="Submit"><br>';
echo '</form>';
?>

