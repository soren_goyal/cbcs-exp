<?php

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	require_once("initialize.php");
	$subid = $_SESSION["SubjectID"];

	$sql = "INSERT INTO `Life Events Checklist Respones` (`Subject ID`) VALUES ($subid) ";
	$result = $conn->query($sql);
	if(!$result)
	{
	var_dump($_POST);
	echo $conn->error;
	echo "Insertion Unsuccessful:".$sql."<br>";
	}
	
	for ($i=1; $i<=17 ; $i++) {

	$sql = "UPDATE `Life Events Checklist Respones` SET `$i`=\"".$_POST[$i]."\" WHERE `Subject ID`=$subid";
	$result = $conn->query($sql);
	if(!$result)
	{
		var_dump($_POST);
	    echo $conn->error;
	    echo "Insertion Unsuccessful:".$sql."<br>";
	}
	}

	$_SESSION['step'] = 1.4;
	header("Location:../index.php");
}
?>

<!DOCTYPE HTML>
<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>

<div id="main">     
    <div id="content">
    	<h1>Life Events Checklist</h1>
    	<p>Listed below are a number of difficult or stressful things that sometimes happen to people.
    	For each event check one or more of the boxes to the right to indicate that:</p>
    	<ul>
			<li>it happened to you personally</li>
			<li>you witnessed it happen to someone else</li>
			<li>you learned bout it happening to someone close to you</li>
			<li>you are not sure it if fits</li>
			<li>it does not apply to you</li>
		</ul>
		<p>Be sure to consider your entire life (growing up as well as adulthood) as you go through the list of events.</p>
		<form name="htmlform" method="post" action="" autocomplete="off">
		
		<table style="width:100%">  
		  <tr>
		  	<td></td>
		  	<td style="text-align:center"><h4>Event</h4></td>
		  	<td style="text-align:center"><h4>Happened to me</h4></td>
		  	<td style="text-align:center"><h4>Witnessed it</h4></td>
		  	<td style="text-align:center"><h4>Learned about it</h4></td>
		  	<td style="text-align:center"><h4>Not sure</h4></td>
		  	<td style="text-align:center"><h4>Does not apply</h4></td>
		  </tr>
		  <?php
		  $event = array(
		  	"Natural disaster (for example, flood, hurricane, tornado, earthquake).",
		  	"Fire or Explosion",
		  	"Transportation accident (for example, car accident, boat accident, train wreck, plane crash.",
		  	"Serious accident at work, home or during recreational activity.",
		  	"Exposure to toxic substance (for example, dangerous chemicals, radiation).",
		  	"Physical Assualt (for example, being attacked, hit, slapped,kicked, beaten up)",
		  	"Assault with a wepon (for example, being shot, stabbed, threatened with a knife, gun or bomb)",
		  	"Sexual assualt (rape, attempted rape, made to perform any type of sexual act through force or threat of harm.",
		  	"Other unwanted or uncomfortable sexual experiences.",
		  	"Cobat or exposure to war-zone (in military or as a civillian)",
		  	"Captivity r exposure to a war-zone (in the military or as a civillian)",
		  	"Life-threatening illness or injury",
		  	"Severe human suffering",
		  	"Sudden, violent death (for exmple, homicide, suicide)",
		  	"Sudden unexpecteddeath of someone close to you",
		  	"Serious injury, harm or death you caused to someone else",
		  	"Any other very stressful event or experience."
			);
		  for ($i=1; $i <= 17; $i++) { 
		   echo '<tr>
		  	<td>'.$i.'</td>
		    <td>'.$event[$i-1].'</td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="Happened to me"></td> 
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="Witnessed it"></td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="Learned about it" checked></td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="Not sure"></td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="Does not apply"></td>
		  </tr>';
		}
		echo '</table>';
		?>
		<p style="padding-top: 15px;text-align:center "><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Submit" id = "mySubmit"/></p>
		</form>
	</div>
</div>';
<?php
    require_once("../layout/footer.php");
?>    
</body>
</html>
