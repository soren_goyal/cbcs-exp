<!DOCTYPE HTML>
<html>
<head>
<?php 
    require_once("../layout/experiment-header.php");
    require_once("experiment-initialize.php");
?>
</head>
<body>
<!-- =========================================================================================== -->
<?php
if(isset($_POST["expression_number"]))
{
    $sql = "SELECT MAX(`Stimulus ID`) AS N FROM `Training Images`";
    $result = $conn->query($sql);
    if($conn->error != NULL)
        echo $sql."<br>ERROR:".$conn->error."<br>";
    $row = $result->fetch_array(MYSQLI_ASSOC);
    if($_POST["expression_number"] >= $row["N"]) // Check to see of all expressions have been displayed or not
    {    
        $_SESSION["experiment_step"] = 2; //Check if this works or not
        header("Location:experiment-main.php");
    }
    else
        $expnumber = $_POST["expression_number"] + 1;
}
else
    $expnumber = 1;     
echo '<div class="container">';
    echo '<div class="row centered">';
        echo '<div class="col-sm-8 centered">'; 
            $sql = "SELECT `Address` FROM `Training Images` WHERE `Stimulus ID`=".$expnumber;
            //echo $sql;
            $result = $conn->query($sql);
            if($conn->error != NULL)
                echo $sql."<br>ERROR:".$conn->error."<br>";
            if($result->num_rows == 0)
                die("No Training Images Found");
            else
                $row = $result->fetch_array(MYSQLI_ASSOC);
            echo "<img src=../../resources/db/training/" .$row["Address"].">";
        echo '</div>';
        echo '<div class="col-sm-4 centered">';
            $sql = "SELECT `Name` FROM `Expressions` WHERE `Expression ID`=$expnumber";
            $result = $conn->query($sql);
            if($result->num_rows == 0)
                die("No Expressions Found.<br>".$conn->error);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            echo "Expression:".$row["Name"]."<br>";
            echo 'Expression No :'.$expnumber.'/22';
            echo '<form  method="post" action="'.$_SERVER['PHP_SELF'].'">';
                        echo "<button name=\"response\" type=\"submit\" class=\"btn btn-experiment\" value = \"Next\">Next</button>";
                        echo "<input type=\"hidden\" name=\"expression_number\" value=\"".$expnumber."\">";
            echo '</form>
        </div>
    </div>
</div>';
?>
<!-- =========================================================================================== -->  
<div class="container">
    <div class="row centered">
        <div class="col-sm-8 centered">
        </div>
        <div class="col-sm-4 centered">
            <?php require_once("countdown.php"); ?>
        </div>
    </div>
</div>

      
</body>
</html>
