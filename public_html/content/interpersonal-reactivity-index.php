<?php

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	require_once("initialize.php");
	$subid = $_SESSION["SubjectID"];

	$sql = "INSERT INTO `Interpersonal Reactivity Index Responses` (`Subject ID`) VALUES ($subid) ";
	$result = $conn->query($sql);
	if(!$result)
	{
	var_dump($_POST);
	echo $conn->error;
	die("Insertion Unsuccessful:".$sql."<br>");
	}
	
	for ($i=1; $i<=17 ; $i++) {

		$sql = "UPDATE `Interpersonal Reactivity Index Responses` SET `$i`=".$_POST[$i]." WHERE `Subject ID`=$subid";
		$result = $conn->query($sql);
		if(!$result)
		{
			var_dump($_POST);
		    echo $conn->error;
		    die("Update Unsuccessful:".$sql."<br>");
		}
	}

	$_SESSION['step'] = 2;
	header("Location:../index.php");
}
?>

<!DOCTYPE HTML>
<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>

<div id="main">     
    <div id="content">
    	<h1>INTERPERSONAL REACTIVITY INDEX </h1>
    	<p>The following statements inquire about your thoughts and feelings in a variety of situations.
    	For each item, indicate how well it describes you by choosing the appropriate letter on the scale at
    	 the top of the page: A, B, C, D, or E. When you have decided on your answer, fill in the letter on
    	the answer sheet next to the item number. READ EACH ITEM CAREFULLY BEFORE RESPONDING. 
    	Answer as honestly as you can.</p>
		<u>ANSWER SCALE</u>:<br>
		Does not describe be very well 1 2 3 4 5 Describes me very well
		<form name="htmlform" method="post" action="" autocomplete="off">
		
		<table style="width:100%">  
		  <tr>
		  	<td width="3%"></td>
		  	<td style="text-align:center" width="47%"><h4></h4></td>
		  	<td style="text-align:center" width="10%"><h4>Does not describe be very well</h4></td>
		  	<td style="text-align:center" width="10%"><h4></h4></td>
		  	<td style="text-align:center" width="10%"><h4></h4></td>
		  	<td style="text-align:center" width="10%"><h4></h4></td>
		  	<td style="text-align:center" width="10%"><h4>Describes me very well</h4></td>
		  </tr>
		  <?php
		  $event = array(
		  	"I daydream and fantasize, with some regularity, about things that might happen to me. ",
		  	"I often have tender, concerned feelings for people less fortunate than me. ",
		  	"I sometimes find it difficult to see things from the \"other guy's\" point of view.",
		  	"Sometimes I do not feel very sorry for other people when they are having problems.",
		  	"I really get involved with the feelings of the characters in a novel",
		  	"In emergency situations, I feel apprehensive and ill-at-ease. ",
		  	"I am usually objective when I watch a movie or play, and I don't often get completely caught up in it. ",
		  	"I try to look at everybody's side of a disagreement before I make a decision. ",
		  	"When I see someone being taken advantage of, I feel kind of protective towards them.",
		  	"I sometimes feel helpless when I am in the middle of a very emotional situation.",
		  	"I sometimes try to understand my friends better by imagining how things look from their perspective. ",
		  	"Becoming extremely involved in a good book or movie is somewhat rare for me.",
		  	"When I see someone get hurt, I tend to remain calm. (PD) (-) 14. Other people's misfortunes do not usually disturb me a great deal.",
		  	"Other people's misfortunes do not usually disturb me a great deal.",
		  	"If I'm sure I'm right about something, I don't waste much time listening to other people's arguments.",
		  	"After seeing a play or movie, I have felt as though I were one of the characters.",
		  	"Being in a tense emotional situation scares me.",
		  	"When I see someone being treated unfairly, I sometimes don't feel very much pity for them.",
		  	"I am usually pretty effective in dealing with emergencies.",
		  	"I am often quite touched by things that I see happen. ",
		  	"I believe that there are two sides to every question and try to look at them both. ",
		  	"I would describe myself as a pretty soft-hearted person.",
		  	"When I watch a good movie, I can very easily put myself in the place of a leading character. ",
		  	"I tend to lose control during emergencies.",
		  	"When I'm upset at someone, I usually try to \"put myself in his shoes\" for a while.",
		  	"When I am reading an interesting story or novel, I imagine how l would feel if the events in the story were happening to me.",
		  	"When I see someone who badly needs help in an emergency, I go to pieces.",
		  	"Before criticizing somebody, I try to imagine how I would feel if I were in their place."
			);
		  for ($i=1; $i <= 28; $i++) { 
		   echo '<tr>
		  	<td width="3%">'.$i.'</td>
		    <td width="47%">'.$event[$i-1].'</td>
		    <td style="text-align:center" width="10%"><input type="radio" name="'.$i.'" value="1">1</td> 
		    <td style="text-align:center" width="10%"><input type="radio" name="'.$i.'" value="2">2</td>
		    <td style="text-align:center" width="10%"><input type="radio" name="'.$i.'" value="3" checked>3</td>
		    <td style="text-align:center" width="10%"><input type="radio" name="'.$i.'" value="4">4</td>
		    <td style="text-align:center" width="10%"><input type="radio" name="'.$i.'" value="5">5</td>
		  </tr>';
		}
		echo '</table>';
		?>
		<p style="padding-top: 15px;text-align:center "><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Submit" id = "mySubmit"/></p>
		</form>
	</div>
</div>';
<?php
    require_once("../layout/footer.php");
?>    
</body>
</html>
