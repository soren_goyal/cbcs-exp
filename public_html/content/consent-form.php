<!DOCTYPE HTML>
<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>
<div id="main">     
    <div id="content">
		<?php
			require_once("initialize.php");
		?>
		<h1> Online Experiment Participation </h1>
			
		<h3> <!--The Ohio State University Consent to Participate in Research -->

		Study Title:	Characterizing Face Processing in  Psychiatric Disorders <br>
		Protocol Number: 2013H0145<br>
		Principal Investigator:	Aleix Martinez <br>
		Sponsor	National Eye Institute </h3>
		<h3>Please Read Consent Form Below <a href="consent.pdf">(Download PDF)</a></h3> 
		<div id="" style="overflow-y: scroll; height:500px; width:650px;font-size:15px">
		<p>
		<ul>
		<li>This is a consent form for research participation.  It contains important information about this study and what to expect if you decide to participate.  Please consider the information carefully. Feel free to discuss the study with your friends and family and to ask questions before making your decision whether or not to participate.</li>
		<li> Your participation is voluntary.  You may refuse to participate in this study.  If you decide to take part in the study, you may leave the study at any time.  No matter what decision you make, there will be no penalty to you and you will not lose any of your usual benefits.  Your decision will not affect your future relationship with The Ohio State University.  If you are a student or employee at Ohio State, your decision will not affect your grades or employment status.</li>
		<li>You may or may not benefit as a result of participating in this study.  Also, as explained below, your participation may result in unintended or harmful effects for you that may be minor or may be serious depending on the nature of the research.</li>
		<li>You will be provided with any new information that develops during the study that may affect your decision whether or not to continue to participate.  If you decide to participate, you will be asked to sign this form and will receive a copy of the form.  You are being asked to consider participating in this study for the reasons explained below.  </li>
		</ul>
		<ol type="1">
		<li>   Why is this study being done?<br>
		The purpose of this study is to understand the potential impact of a psychiatric disorder (e.g., post-traumatic stress disorder, depression, autism spectrum disorders) on the ability to process facial expressions of emotion.  You are invited to participate in this research study to assist in this learning.</li>

		<li>How many people will take part in this study?<br>
		We expect 1000 people to participate in this study.  </li>

		<li>What will happen if I take part in this study?<br>
		If you volunteer to participate in this study, we would ask you to do the following things:

		You will be asked to fill out a survey with demographic (e.g., age, gender, ethnicity) and mental health information.

		Then, you will view different sets of faces on a computer display. You will be asked to make judgments about these faces which may include categorization or ratings. Responses will be made using a computer keyboard.  

		We may also like to invite you to participate in future research. Please check the appropriate box below. Your contact information will be stored on the Computational Biology and Cognitive Science lab’s secure server or a secure encrypted external hard drive. You may withdraw your approval at any time using the contact information provided at the end of this form or on our lab website (http://cbcsl.ece.ohio-state.edu/).</li>
		<li>    How long will I be in the study? <br>

		Although the experiment is self-paced, we expect it to take approximately 10 minutes.</li>
		<li> 	Can I stop being in the study?<br>

		You may leave the study at any time.  You also do not have to answer any questions that make you feel uncomfortable. If you decide to stop participating in the study, there will be no penalty to you, and you will not lose any benefits to which you are otherwise entitled.  Your decision will not affect your future relationship with The Ohio State University. </li>


		<li>    What risks, side effects or discomforts can I expect from being in the study?<br>

		Breach of confidentiality is a risk as the questionnaire may contain sensitive information such previous diagnosis of a psychiatric disorder. To maintain confidentiality, a code will be used for each participant. For further information on confidentiality measures, see point 9 below.  

		Dry eyes or mild eye strain are also risks due to the extended viewing of a computer display.  You may also get bored.</li>

		<li>    What benefits can I expect from being in the study?<br>

		There are no direct benefits to you for participating in this study other than the satisfaction of knowing you participated in research. </li>

		<li>    What other choices do I have if I do not take part in the study?<br>

		You may choose not to participate without penalty or loss of benefits to which you are otherwise entitled.</li>

		<li>   Will my study-related information be kept confidential?<br>

		Efforts will be made to keep your study-related information confidential.  All digital records will be kept on the Computational Biology and Cognitive Science lab’s secure server or a secure encrypted external hard drive.  

		However, there may be circumstances when this information must be released.  For example, personal information regarding your participation in this study may be disclosed if required by state law. <br>
		Also, your records may be reviewed by the following groups:
		<ul> 
		<li>	Office for Human Research Protections or other federal, state, or international regulatory agencies;</li>
		<li>	The Ohio State University Institutional Review Board or Office of Responsible Research Practices;</li>
		<li>	The sponsor supporting the study, their agents or study monitors.</li>
		</ul>
		You may also be asked to sign a separate Health Insurance Portability and Accountability Act (HIPAA) research authorization form if the study involves the use of your protected health information.</li>


		<li> What are the costs of taking part in this study?<br>

		There are no costs to participating in this study.</li>


		<li> Will I be paid for taking part in this study?<br>

		You will not receive payment for participation.</li>

		<li> What are my rights if I take part in this study?<br>

		If you choose to participate in the study, you may discontinue participation at any time without penalty or loss of benefits.  By signing this form, you do not give up any personal legal rights you may have as a participant in this study.

		If you agree to allow us to contact you for future studies, you may withdraw your approval at any time using the contact information provided at the end of this form or on our lab website (http://cbcsl.ece.ohio-state.edu/). 

		You will be provided with any new information that develops during the course of the research that may affect your decision whether or not to continue participation in the study.

		You may refuse to participate in this study without penalty or loss of benefits to which you are otherwise entitled.

		An Institutional Review Board responsible for human subjects research at The Ohio State University reviewed this research project and found it to be acceptable, according to applicable state and federal regulations and University policies designed to protect the rights and welfare of participants in research.</li>


		<li>Who can answer my questions about the study?<br>

		For questions, concerns, or complaints about the study you may contact:
		Aleix Martinez (aleix@ece.osu.edu).

		For questions about your rights as a participant in this study or to discuss other study-related concerns or complaints with someone who is not part of the research team, you may contact Ms. Sandra Meadows in the Office of Responsible Research Practices at 1-800-678-6251.

		If you are injured as a result of participating in this study or for questions about a study-related injury, you may contact Aleix Martinez (aleix@ece.osu.edu).</li>
		</ol>
		</p>
		</div>
		<h3>Signing the consent form</h3>
		<p><span style="font-size:17px">
		I have read this form and I am aware that I am being asked to participate in a research study.<br> 
		I voluntarily agree to participate in this study. <br>
		I am not giving up any legal rights by signing this form. </span>

		<form name="form" method="post" action="consent-form-save.php" onsubmit="return validateForm(this)">
			<p><input type="checkbox" name="checkbox" > Agree to Terms </p>
			<p>Sign with Name<input class="contact" type="text" name="name" value="" autofocus="off" required ></p>
			<p>E-mail Address<input class="contact" type="email" name="email" value="" required ></p>
			<p>Confirm E-mail Address</span><input class="contact" type="email" name="cemail" value=""  required ></p>           
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="contact_submitted" value="submit" id = "mySubmit"/>
		</p>
		</form>

		<script type = "text/javascript">

		function checkSubmit(ele, id) {
			x = document.getElementById(id);
			if (ele.checked == true) x.disabled = false;
			else x.disabled = true;
		}

		function validateForm(form) {
			var email = form.elements['email'];
			var cemail = form.elements['cemail'];
			
			if (email.value != cemail.value) {
				alert("E-mail Addresses Don't Match");
				return false;
			}
			if(form.elements['checkbox'].checked == false) {
				alert("Agree to Terms by Checking the checkbox");
				return false;
			}
		}
		</script>
    </div>
</div>
<?php    
    require_once("../layout/footer.php");
?>
    
</body>

</html>


