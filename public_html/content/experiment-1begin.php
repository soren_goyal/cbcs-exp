<!DOCTYPE HTML>
<html>
<head>
<?php 
	require_once("../layout/experiment-header.php");
	require_once("experiment-initialize.php");
?>
</head>
<body>
<!-- =========================================================================================== -->
<?php
	if(isset($_POST["begin"]))
	{
		$_SESSION["experiment_step"] = 1;
		header("Location:experiment-main.php");
	}
?>
<div class="jumbotron" style="background-color:#000">
	<h1>Recognize the Facial Expressions</h1>
	<p>Before we begin the experiment, we will show you all the diffrent types of expressions and their name.
	Click to Continue.</p>
	<form method="post" action="">
		<input type="hidden" name="begin" value="1">
		<input class="submit btn btn-experiment btn-lg" type="submit" value="Let's Begin">
	</form>
</div>
<!-- =========================================================================================== -->  
<div class="container">
    <div class="row centered">
        <div class="col-sm-8 centered">
        </div>
        <div class="col-sm-4 centered">
            <?php require_once("countdown.php"); ?>
        </div>
    </div>
</div>

      
</body>
</html>
