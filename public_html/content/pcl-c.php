<?php

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	require_once("initialize.php");
	$subid = $_SESSION["SubjectID"];

	$sql = "INSERT INTO `PCL-C Responses` (`Subject ID`) VALUES ($subid) ";
	$result = $conn->query($sql);
	if(!$result)
	{
	echo $conn->error;
	die("Insertion Unsuccessful:".$sql."<br>");
	}
	var_dump($_POST);
	for ($i=1; $i<=17 ; $i++) {

		$sql = "UPDATE `PCL-C Responses` SET `$i`=".$_POST[$i]." WHERE `Subject ID`=$subid";
		$result = $conn->query($sql);
		if(!$result)
		{
		    echo $conn->error;
		    die("Update Unsuccessful:".$sql."<br>");
		}
	}

	$_SESSION['step'] = 1.5;
	header("Location:../index.php");
}
?>

<!DOCTYPE HTML>
<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>

<div id="main">     
    <div id="content">
    	<h1>PCL-C</h1>
    	<p><u>Instructions</u>: Below is a list of problems and complaints that people sometimes have in response to stressful life experiences. 
    		Please read each one carefully, then circle one of the numbers to the right to indicate how much 
    		you have been bothered by that problem in the past month. </p>
		<form name="htmlform" method="post" action="" autocomplete="off">
		
		<table style="width:100%">  
		  <tr>
		  	<td></td>
		  	<td style="text-align:center"><h4></h4></td>
		  	<td style="text-align:center"><h4>Not at all</h4></td>
		  	<td style="text-align:center"><h4>A little bit</h4></td>
		  	<td style="text-align:center"><h4>Moderately</h4></td>
		  	<td style="text-align:center"><h4>Quite a bit</h4></td>
		  	<td style="text-align:center"><h4>Extreme</h4></td>
		  </tr>
		  <?php
		  $event = array(
		  	"Repeated, disturbing memories, thoughts, or images of a stressful experience from the past? ",
		  	"Repeated, disturbing dreams of a stressful experience from the past? ",
		  	"Suddenly acting or feeling as if a stressful experience were happening",
		  	"Feeling very upset when something reminded you of a stressful experience from the past? ",
		  	"Having physical reactions (e.g., heart pounding, trouble breathing, sweating) when something reminded you of a stressful experience from the past?",
		  	"Avoiding thinldng about or talking about a stressful experience from the past or avoiding having frelings related to it?",
		  	"Avoiding activities or situations because they reminded you of a stressful experience from the past? ",
		  	"Trouble remembering important parts of a stressful experience from the past?",
		  	"Loss of interest in activities that you used to enjoy?",
		  	"Feeling distant or cut off from other people?",
		  	"Feeling emotionally numb or being unable to have loving feelings for those close to you?",
		  	"Feeling as if your future will somehow be cut short?",
		  	"Trouble falling or staying asleep? ",
		  	"Feeling initable or having angry outbursts?",
		  	"Having difficulty concentrating?",
		  	"Being 'super-alert' or watchful or on guard?",
		  	"Feeling jumpy or easily startled?"
			);
		  for ($i=1; $i <= 17; $i++) { 
		   echo '<tr>
		  	<td>'.$i.'</td>
		    <td>'.$event[$i-1].'</td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="1">1</td> 
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="2">2</td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="3" checked>3</td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="4">4</td>
		    <td style="text-align:center"><input type="radio" name="'.$i.'" value="5">5</td>
		  </tr>';
		}
		echo '</table>';
		?>
		<p style="padding-top: 15px;text-align:center "><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Submit" id = "mySubmit"/></p>
		</form>
	</div>
</div>';
<?php
    require_once("../layout/footer.php");
?>    
</body>
</html>
