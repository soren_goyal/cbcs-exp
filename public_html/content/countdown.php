<?php
	if(isset($_GET["sessionend"]))
	{
		$_SESSION["experiment_step"] = 4;
        header("Location:experiment-main.php");
        exit();
    }
?>
<div id="clockdiv">
    Time Remaining:
    <span class="minutes"></span>:<span class="seconds"></span>
</div>

<script>
function getTimeRemaining(endtime) {
	var timeelapsed = endtime - (new Date).getTime()/1000 ;
	
	//console.log(window.location.href.concat("?sessionend=1"));
	if (timeelapsed <= 0)
		window.location = window.location.href.concat("?sessionend=1");
	var minutes = Math.floor(timeelapsed/60);
	var seconds = Math.floor(timeelapsed%60);
	return {
		'total': timeelapsed,
		'minutes': minutes,
    	'seconds': seconds
	};
}

function initializeClock(id, endtime) {
	var clock = document.getElementById(id);
  	var minutesSpan = clock.querySelector('.minutes');
  	var secondsSpan = clock.querySelector('.seconds');
	function updateClock() {
		var t = getTimeRemaining(endtime);
    	minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    	secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

		if(t.total<=0) {
    		clearInterval(timeinterval);
   		}
	}
	updateClock();
	var timeinterval = setInterval(updateClock,1000);
}


initializeClock('clockdiv', <?php echo $_SESSION['end time'] ?>);

</script>