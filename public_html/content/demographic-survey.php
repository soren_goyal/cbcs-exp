<!DOCTYPE HTML>
<html>
<?php
    require_once("../layout/head.php");
?>

<body>
<?php
    require_once("../layout/header.php"); 
    require_once("../layout/sidebar.php");
?>
<div id="main">     
    <div id="content">
	<h2>Fill in your details</h2>
	<form name="htmlform" method="post" action="demographic-survey-save.php" autocomplete="off">
		<p>Age:<br>
		<input type="number" name="age" value="" autofocus="off" required >
		</p>
		<p><b>Gender</b>:<br>
		<input type="radio" name="gender" value="male" >Male<br>
		<input type="radio" name="gender" value="female"/>Female<br>
		<input type="radio" name="gender" value="na" checked/>Do not wish to Answer
		</p>
		<p><b>Ethinicity</b>:<br>
		<input type="radio" name="ethnicity" value="hispanic">Hispanic or Latino<br>
		<input type="radio" name="ethnicity" value="not hispanic">Not Hispanic or Latino<br>
		<input type="radio" name="ethnicity" value="na" checked>Do not wish to disclose<br> 
		</p>
		<p><b>Race</b>:<br>
		<input type="radio" name="race" value="american indian/alaska native">American Indian/Alaska Native<br>
		<input type="radio" name="race" value="asian">Asian<br>
		<input type="radio" name="race" value="native hawaiian or other pacific islander">Native Hawaiian or Other Pacific Islander<br> 
		<input type="radio" name="race" value="black or african american">Black or African American<br>
		<input type="radio" name="race" value="White">White<br>
		<input type="radio" name="race" value="na" checked>Do not wish to disclose<br> 
		</p>
		<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="submit" id = "mySubmit"/></p>
	</form>
 	</div>
</div>
<?php    
    require_once("../layout/footer.php");
?>    
</body>
</html>

