<div class="sidebar">
 <!-- insert your sidebar items here -->
<h3>Currently Recruiting</h3>
    <h4>Emotion Processing in PTSD and ASD</h4>
        <h5>STILL RECRUITING!</h5>
        <h5>September 15, 2015</h5>
        <p>Seeking participants with PTSD or ASD (Asperger's ok). Participants must be 18 yrs or older with normal or corrected-to-normal vision.<br/>
            <a href="research.html#ptsd">Read more</a></p> <!-- TODO:Correct this link -->
    <h4>Neural representations of facial expressions of emotion</h4>
        <h5>April 3, 2014</h5>
        <p>Seeking participants 18 yrs or older with normal or corrected-to-normal vision. <br/>
            <a href="research.html#EmoFMRI">Read more</a>s</p>		
<h3>Useful Links</h3>
    <ul>         
      <li><a href="http://www.nad.org/issues/american-sign-language/what-is-asl">What is American Sign Language?</a></li>
      <li><a href="http://www.nimh.nih.gov/health/publications/post-traumatic-stress-disorder-ptsd/complete-index.shtml">What is Post-traumatic Stress Disorder?</a></li>
      <li><a href="http://www.nichd.nih.gov/health/topics/asd.cfm">What are Autism Spectrum Disorders?</a></li>
      <li><a href=" http://autism.answers.com">Looking for more on Autism?</a></li>
      <li><a href="http://www.bestcolleges.com/financial-aid/disabled-students">Scholarships for Students with Disorders or Disabilities</a></li>
      <li><a href="http://www.nad.org/">National Association of the Deaf</a></li>
      <li><a href="http://www.expertise.com/home-and-garden/home-remodeling-for-disability-and-special-needs"> Guide to Home Remodeling for Disability and Special Needs</a></li>
      <li><a href="http://studysearch-prod.bmi.osumc.edu/">External experiment participation opportunities</a></li>
 	  <li><a href="http://ece.osu.edu/">Department of Electrical and Computer Engineering</a></li>
      <li><a href="http://www.osu.edu">The Ohio State University</a></li>
    </ul>
<h3>Contact Info</h3>
    <p>469 Dreese Labs<br>
    2015 Neil Ave<br>
    Columbus, OH 43210<br></p>	
</div>
