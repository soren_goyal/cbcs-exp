<title>Computational Biology and Cognitive Science Lab</title>
  <meta name="description" content="Computational Biology and Cognitive Science Lab" />
  <meta name="keywords" content="emotion, language, aleix martinez, face processing, recognition, fMRI, ohio state university" />
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/experiment.css" />